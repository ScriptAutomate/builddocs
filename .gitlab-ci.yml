---
stages:
  - pre-commit
  - docker-images
  - build-docs
  - pages

default:
  before_script:
    - 'chmod -R g-w,o-w . && umask 0022'

include:
  - project: 'saltstack/pop/cicd/ci-templates'
    file: '/lint/pre-commit-run-all.yml'
  - project: 'saltstack/pop/cicd/ci-templates'
    file: '/docker/kaniko-single.yml'

.dockertags: &dockertags
  tags:
  - docker

docker-image:
  <<: *dockertags
  stage: docker-images
  extends: .kaniko-build
  variables:
    CONTAINER_NAME: builddocs
    CONTAINER_TAG: '3.8.6'
    CONTEXT_PATH: '/'
    DOCKERFILE_PATH: 'container/Dockerfile'
  only:
    changes:
      - container/*
      - .gitlab-ci.yml
  except:
    - schedules

master-html:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    DOC_BRANCH: master
    WEBSITE_POINT_RELEASE: master
    WEBSITE_RELEASE: master
  script:
    - ./build_html.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_html.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

master-pdf:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    DOC_BRANCH: master
    WEBSITE_POINT_RELEASE: master
    WEBSITE_RELEASE: master
  script:
    - ./build_pdf.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_pdf.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

master-epub:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    DOC_BRANCH: master
    WEBSITE_POINT_RELEASE: master
    WEBSITE_RELEASE: master
  script:
    - ./build_epub.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_epub.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

latest-html:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: latest
    DOC_BRANCH: 'v3003.2'
    WEBSITE_POINT_RELEASE: '3003.2'
    WEBSITE_RELEASE: latest
  script:
    - ./build_html.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_html.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

latest-pdf:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: latest
    DOC_BRANCH: 'v3003.2'
    WEBSITE_POINT_RELEASE: '3003.2'
    WEBSITE_RELEASE: latest
  script:
    - ./build_pdf.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_pdf.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

latest-epub:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: latest
    DOC_BRANCH: 'v3003.2'
    WEBSITE_POINT_RELEASE: '3003.2'
    WEBSITE_RELEASE: latest
  script:
    - ./build_epub.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_epub.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

latest-3003-html:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: latest
    DOC_BRANCH: 'v3003.2'
    WEBSITE_POINT_RELEASE: '3003.2'
    WEBSITE_RELEASE: '3003'
  script:
    - ./build_html.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_html.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

previous-3002-html:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: previous
    DOC_BRANCH: 'v3002.6'
    WEBSITE_POINT_RELEASE: '3002.6'
    WEBSITE_RELEASE: '3002'
  script:
    - ./build_html.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_html.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

previous-3002-pdf:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: previous
    DOC_BRANCH: 'v3002.6'
    WEBSITE_POINT_RELEASE: '3002.6'
    WEBSITE_RELEASE: '3002'
  script:
    - ./build_pdf.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_pdf.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

previous-3002-epub:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: previous
    DOC_BRANCH: 'v3002.6'
    WEBSITE_POINT_RELEASE: '3002.6'
    WEBSITE_RELEASE: '3002'
  script:
    - ./build_epub.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_epub.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

previous-3001-html:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: previous
    DOC_BRANCH: 'v3001.7'
    WEBSITE_POINT_RELEASE: '3001.7'
    WEBSITE_RELEASE: '3001'
    PREVIOUS_RELEASE: "${WEBSITE_POINT_RELEASE}"
    PREVIOUS_RELEASE_DIR: "${WEBSITE_RELEASE}"
  script:
    - ./build_html.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_html.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

previous-3001-pdf:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: previous
    DOC_BRANCH: 'v3001.7'
    WEBSITE_POINT_RELEASE: '3001.7'
    WEBSITE_RELEASE: '3001'
    PREVIOUS_RELEASE: "${WEBSITE_POINT_RELEASE}"
    PREVIOUS_RELEASE_DIR: "${WEBSITE_RELEASE}"
  script:
    - ./build_pdf.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_pdf.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

previous-3001-epub:
  stage: build-docs
  image: ${CI_REGISTRY_IMAGE}/builddocs:3.8.6
  variables:
    BUILD_TYPE: previous
    DOC_BRANCH: 'v3001.7'
    WEBSITE_POINT_RELEASE: '3001.7'
    WEBSITE_RELEASE: '3001'
    PREVIOUS_RELEASE: "${WEBSITE_POINT_RELEASE}"
    PREVIOUS_RELEASE_DIR: "${WEBSITE_RELEASE}"
  script:
    - ./build_epub.sh
    - if [ "${CI_COMMIT_REF_PROTECTED}" == "true" ]; then ./publish_epub.sh; fi
  cache:
    key: venv-${DOC_BRANCH}
    paths:
      - venv/
  artifacts:
    paths:
      - public/
    expire_in: 7 days

pages:
  stage: pages
  image: alpine
  script:
    - echo '<meta http-equiv="refresh" content="0; url=en/latest/" />' > ./public/index.html
    - echo '<meta http-equiv="refresh" content="0; url=contents.html" />' > ./public/en/latest/index.html
    - find public -maxdepth 3
    - 'echo "Gitlab Pages available at: ${CI_PAGES_URL}"'
  artifacts:
    paths:
      - public/
